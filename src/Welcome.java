/*
Name: Eric Filishtiner
Date: 8/24/2019
Class: CS3
Description: Displays a welcome message to the user
*/

import javax.swing.JOptionPane;
public class Welcome 
{
public static void main(String[] args)
	{
	System.out.print("Welcome to Java programming");
	JOptionPane.showMessageDialog(null, "Welcome to Java Programming");
	}//end main
}

